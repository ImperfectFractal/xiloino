**Xilono 2.0**

Vídeo do funcionamento: https://youtu.be/RGZ1o38bmYU

"Top view" do circuito: https://youtu.be/PJDVTXxSMUw 

**Descrição:**

Um instrumento musical feito com um Arduino Mega, protobord, 8 LEDs verdes e 5 amarelos, 1 buzzer, 13 resistores de 100 Ohms e fios tipo jumper. Ele é capaz de produzir 13 notas distintas e possui 12 efeitos básicos de som.
O piano e o xilofone foram fortes inspirações para criação desse projeto.

**Modo de uso:**

(Orientação adotada para protoboard: LEDs devem estar voltados para longe do usuário)
Quando o jumper é conectado numa saída 5 V e posto em contato com a perna esquerda de um dos LEDs, esse acenderá e uma nota será emitida pelo buzzer.

Cada LED corresponde a uma nota musical. Esse pequeno teclado cobre uma oitava: o LED verde mais à esquerda e à direita são Dó. Notas à esquerda são mais graves. Por utilizar uma escala diatônica, o intervalo de uma nota para outra é de um semitom.
Os LEDs verdes equivalem as teclas brancas de um piano, enquanto os amarelos, as teclas pretas.

Os efeitos sonoros podem ser selecionados após entrar no menu de seleção. Há duas sequências possíveis para entrar nesse menu, que podem ser tocadas da seguinte forma:

(Da esquerda para direita)
1º – 2º – 3º – 3º – 2º – 1º, ou apenas 1º – 2º – 3º – 2º – 1º, LEDs. 

Posteriormente ao Dó – Ré – Mi – Ré – Dó, um sinal sonoro indicam que o instrumento entrou no modo menu. Nesse modo, cada LED corresponde a um efeito. Abaixo segue a lista completa deles:

(Da esquerda para direta)

1º – Sem efeito, som padrão; 
2º – Seno; 
3º – Cosseno; 
4º – Tangente; 
5º – Raiz quadrada; 
6º – Logaritmo; 
7º - “Random”; 
8º - “Mix”; 
9º - “Same”; 
10º - “Strange”; 
11º - “Bob”; 
12º - “Antipod”; 
13º – Inverso;
