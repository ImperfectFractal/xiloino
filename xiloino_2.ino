#include <math.h>
#define BUZZ 35
#define EGG 1.059463094
 
int KEYS[13] = {0};
double FREQ[13] = {0};
 
// Sequencia para o menu
const int codigo[6] = {0, 1, 2, 1, 0, -1};

int lastVal = -1;
int push = 0;
int currentEffect = 0;

// Funcoes de effeito

double randf(double) {
  return ((double)random(2048)) / 2048;
}

double mix(double n) {
  return sin(n) * cos(n) * tan(n) * sqrt(n);
}

double same(double n) {
  return n;
}

double strange(double n) {
  delay(random(5,15));
  return n * (random(1,3));
}

double bob(double constructor) {
  return constructor * (tan(random(1,2)));
}

double antipod(double n) {
  return random(0, 2) ? 0.5 : 2;
}

double inverse(double n) {
  return (1/n);
}

// Vetor de ponteiros para os efeitos
double (*eff[13])(double) = {NULL, &sin, &cos, &tan, &sqrt, &log, &randf, &mix, &same, &strange, &bob, &antipod, &inverse};

int getKey() {
  
    for (int i = 0; i < 13; i++)
      if (digitalRead(KEYS[i]))
        return i;     
        
    return -1;
}

void cbeep(int f1, int f2) {
  
  delay(200);
  tone(BUZZ, f1, 100);
  delay(200);
  tone(BUZZ, f2, 100);
  delay(250);

}

// Menu principal
void mainMenu() {
  
  cbeep(440, 880);
  
  while (1) {

    int select = getKey();
    if (select != -1) {

      currentEffect = select;
      cbeep(880, 440);
      return;
    }  
  }
}

void setup() {
  
  for (int i = 0; i < 13; i++) {
    
    FREQ[i] = 440 * pow(EGG, i);
    KEYS[i] = i + 22;
    pinMode(KEYS[i], INPUT);
  }
  
  pinMode(BUZZ, OUTPUT);
  
  tone(BUZZ, 440, 50);
}

void loop() {

  int val = getKey();
  if (val != -1) {

     if (!currentEffect) {
      
      tone(BUZZ, FREQ[val], 125);

     // Toca nota com efeito
     } else {
      
        for (double i = 0.25; i< 1.75; i += 0.25) {
    
          tone(BUZZ, FREQ[val] * eff[currentEffect](i), 125);
          delay(50);
         }
     }

    // Verificacao para acesso do menu
    if (lastVal != val) {

      (val == codigo[push]) ? push++ : push = 0;

      if (push == 5) {
        
        mainMenu();
        lastVal = -1;
        push = 0;
        
      } else {
        lastVal = val; 
      }

    }
   
  }
}
